package impl

import (
	"context"

	"gitlab.com/go-course-project/go15/vblog/apps/token"
	"gitlab.com/go-course-project/go15/vblog/apps/user"
	"gitlab.com/go-course-project/go15/vblog/conf"
	"gorm.io/gorm"
)

func NewTokenServiceImpl(userServiceImpl user.Service) *TokenServiceImpl {
	// 每个业务对象, 都可能依赖到 数据库
	// db = create conn
	// 获取一个全新的 mysql 连接池对象
	// 程序启动的时候一定要加载配置
	return &TokenServiceImpl{
		db: conf.C().MySQL.GetDB(),

		user: userServiceImpl,
	}
}

type TokenServiceImpl struct {
	// db conn 共享对象
	// mysql host port  ....
	db *gorm.DB

	// 依赖用户服务
	user user.Service
}

// 令牌颁发
func (i *TokenServiceImpl) IssueToken(ctx context.Context, in *token.IssueTokenRequest) (*token.Token, error) {
	// 1. 查询用户对象
	// i.QueryUser 是调用的user.Service的接口吗？
	queryUser := user.NewQueryUserRequest()
	queryUser.Username = in.Username
	us, err := i.user.QueryUser(ctx, queryUser)
	if err != nil {
		return nil, err
	}

	// 2. 比对用户密码
	us.Items[0].CheckPassword(in.Password)

	// 3. 颁发一个令牌(token)

	// 4. 把令牌存储在数据库里面

	// 5. 返回令牌
	return nil, nil
}

// 令牌撤销
func (i *TokenServiceImpl) RevolkToken(context.Context, *token.RevolkTokenRequest) (*token.Token, error) {
	// 直接删除数据里面存储的Token
	return nil, nil
}

// 令牌校验, 校验令牌合法性
func (i *TokenServiceImpl) ValidateToken(context.Context, *token.ValidateTokenRequest) (*token.Token, error) {
	// 1. 查询出Token, Where AccessToken来查询

	// 2. 判断Token是否过去, 1. 先判断RefreshToken有没有过期 2. AccessToken有没有过期

	// 3. Token合法: 1. 是我颁发, 2. 没有过期
	// 返回查询到的Token
	return nil, nil
}
